﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        Token token;
        EligibilityInquiry elginq;

        public ActionResult Index()
        {
            // Token call
            GetToken();
            GetEligInq();
            return View();
        }

        private void GetToken()
        {
            // do test token call
            WebRequest request = WebRequest.Create("https://api.pverify.com/Test/Token");
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";
            ((HttpWebRequest)request).ContentType = "application/x-www-form-urlencoded";

            string strNew = "username=pverify_demo";
            strNew += "&password=" + HttpUtility.UrlEncode("pverify@949");
            strNew += "&grant_type=password";

            using (StreamWriter stOut = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII))
            {
                stOut.Write(strNew);
                stOut.Close();
            }

            // Get the response.  
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.  
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.  
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.  
            string responseFromServer = reader.ReadToEnd();

            // put into object

            token = JsonConvert.DeserializeObject<Token>(responseFromServer);

            // Display the content.  
            Console.WriteLine(responseFromServer);
            // Clean up the streams and the response.  
            reader.Close();
            response.Close();
        }

        private void GetEligInq()
        {
            // do test token call
            WebRequest request = WebRequest.Create("https://api.pverify.com/Test/API/EligibilityInquiry");
            request.Headers["Client-User-Name"] = "pverify_demo";
            request.Headers["Authorization"] = "Bearer " + token.access_token;
            request.Method = "POST";
            ((HttpWebRequest)request).ContentType = "application/json";

            string body = CreateRequestBody();
            body = "";
            Debug.WriteLine(body);
            Console.WriteLine(body);
            using (StreamWriter stOut = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII))
            {
                stOut.Write(body);
                stOut.Close();
            }

            // Get the response.  
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.  
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.  
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.  
            string responseFromServer = reader.ReadToEnd();

            // put into object

            elginq = JsonConvert.DeserializeObject<EligibilityInquiry>(responseFromServer);

            // Display the content.  
            Console.WriteLine(responseFromServer);
            // Clean up the streams and the response.  
            reader.Close();
            response.Close();

        }
        private string CreateRequestBody()
        {
            EligRequest req = new EligRequest();
            req.payerCode = "00001";

            Provider prov = new Provider();
            prov.firstName = "";
            prov.lastName = "foo";
            prov.npi = "1234";

            req.provider = prov;

            SubscriberRequest sub = new SubscriberRequest();
            sub.firstName = "test";
            sub.middleName = "";
            sub.lastName = "last";
            sub.dob = "12/31/2018";
            sub.memberID = "1234";

            req.subscriber = sub;

            DependentRequest dep = new DependentRequest();
            PatientRequest pt = new PatientRequest();
            pt.firstName = "";
            pt.middleName = "";
            pt.lastName = "";
            pt.gender = "";

            pt.dob = "01/01/2010";
            dep.patient = pt;
            dep.relationWithSubscriber = "";
            req.dependent = dep;

            req.isSubscriberPatient = "true";
            req.doS_EndDate = "11/30/2018";
            req.doS_StartDate = "11/30/2018";
            List<string> codes = new List<string>();
            codes.Add("30");
            //codes.Add("PT");
            req.serviceCodes = codes;
            req.requestSource = "RestAPI";

            string output = JsonConvert.SerializeObject(req);
            return output;
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    } // main class

    public class EligibilityPeriod
    {
        public object Label { get; set; }
        public string EffectiveFromDate { get; set; }
        public object ExpiredOnDate { get; set; }
    }

    public class Identification
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public object Name { get; set; }
    }

    public class Subscriber
    {
        public object Address1 { get; set; }
        public object Address2 { get; set; }
        public object City { get; set; }
        public object CommunicationNumber { get; set; }
        public object Date { get; set; }
        public object DOB_R { get; set; }
        public string Firstname { get; set; }
        public string Gender_R { get; set; }
        public IList<Identification> Identification { get; set; }
        public string Lastname_R { get; set; }
        public object Middlename { get; set; }
        public object State { get; set; }
        public object Suffix { get; set; }
        public object Zip { get; set; }
    }

    public class DateInfo
    {
        public string Date { get; set; }
        public string Type { get; set; }
    }


    public class DependentInfo
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public object CommunicationNumber { get; set; }
        public IList<DateInfo> Date { get; set; }
        public string DOB_R { get; set; }
        public string Firstname { get; set; }
        public string Gender_R { get; set; }
        public IList<Identification> Identification { get; set; }
        public string Lastname_R { get; set; }
        public object Middlename { get; set; }
        public string State { get; set; }
        public object Suffix { get; set; }
        public string Zip { get; set; }
    }

    public class Dependent
    {
        public DependentInfo DependentInfo { get; set; }
        public string Relationship { get; set; }
    }

    public class DemographicInfo
    {
        public Subscriber Subscriber { get; set; }
        public Dependent Dependent { get; set; }
    }

    public class InNetworkParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public object Message { get; set; }
    }

    public class NetworkSection
    {
        public string Identifier { get; set; }
        public string Label { get; set; }
        public IList<InNetworkParameter> InNetworkParameters { get; set; }
        public object OutNetworkParameters { get; set; }
    }

    public class ServiceParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public IList<string> Message { get; set; }
    }

    public class ServiceTypeSection
    {
        public string Label { get; set; }
        public IList<ServiceParameter> ServiceParameters { get; set; }
    }

    public class HealthBenefitPlanCoverageServiceType
    {
        public string ServiceTypeName { get; set; }
        public IList<ServiceTypeSection> ServiceTypeSections { get; set; }
    }

    public class ServicesType
    {
        public string ServiceTypeName { get; set; }
        public IList<ServiceTypeSection> ServiceTypeSections { get; set; }
    }

    public class ExtensionProperties
    {
        public string PayerCode { get; set; }
        public int PayerID { get; set; }
        public object ResultReport { get; set; }
        public object VerifiedOn { get; set; }
        public object VerifiedBy { get; set; }
    }

    public class EligibilityInquiry
    {
        public int ElgRequestID { get; set; }
        public object EDIErrorMessage { get; set; }
        public string VerificationStatus { get; set; }
        public string VerificationMessage { get; set; }
        public string IsPayerBackOffice { get; set; }
        public string Status { get; set; }
        public string PayerName { get; set; }
        public string VerificationType { get; set; }
        public string DOS { get; set; }
        public string Plan { get; set; }
        public bool IsHMOPlan { get; set; }
        public string ExceptionNotes { get; set; }
        public object AdditionalInformation { get; set; }
        public string OtherMessage { get; set; }
        public string ReportURL { get; set; }
        public EligibilityPeriod EligibilityPeriod { get; set; }
        public DemographicInfo DemographicInfo { get; set; }
        public IList<NetworkSection> NetworkSections { get; set; }
        public HealthBenefitPlanCoverageServiceType HealthBenefitPlanCoverageServiceType { get; set; }
        public IList<ServicesType> ServicesTypes { get; set; }
        public IList<object> CustomFields { get; set; }
        public ExtensionProperties ExtensionProperties { get; set; }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
    }


    public class Provider
    {
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string npi { get; set; }
    }

    
    public class Patient
    {
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
    }


    public class EligRequest
    {
        public string payerCode { get; set; }
        public Provider provider { get; set; }
        public SubscriberRequest subscriber { get; set; }
        public DependentRequest dependent { get; set; }
        public string isSubscriberPatient { get; set; }
        public string doS_StartDate { get; set; }
        public string doS_EndDate { get; set; }
        public IList<string> serviceCodes { get; set; }
        public string requestSource { get; set; }
    }

    public class SubscriberRequest
    {
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string memberID { get; set; }
        public string dob { get; set; }
    }

    public class PatientRequest
    {
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
    }

    public class DependentRequest
    {
        public PatientRequest patient { get; set; }
        public string relationWithSubscriber { get; set; }
    }

}
